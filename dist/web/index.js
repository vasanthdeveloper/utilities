"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.$ = void 0;
const $ = s => (a => (a.length > 1 ? a : a[0]))(document.querySelectorAll(s));
exports.$ = $;
