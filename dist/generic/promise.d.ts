export interface HandlePromiseImpl {
    error?: any;
    returned?: any;
}
export declare const handle: (promiseToHandle: Promise<any>) => Promise<HandlePromiseImpl>;
//# sourceMappingURL=promise.d.ts.map