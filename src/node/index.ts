/*
 *  Utility functions usable in Node.js environment.
 *  Created On 07 July 2021
 */

export * as fs from './fs'
