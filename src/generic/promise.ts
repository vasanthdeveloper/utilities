/*
 *  This file contains a utility function that will suppress a promise's rejection.
 *  Such that we don't need to use Try/Catch and use an if statement instead.
 *  Created On 18 September 2020
 */

export interface HandlePromiseImpl {
    error?: any
    returned?: any
}

export const handle = async (
    promiseToHandle: Promise<any>,
): Promise<HandlePromiseImpl> => {
    return new Promise(r => {
        promiseToHandle
            .catch(error => {
                r({
                    error,
                })
            })
            .then(returned => {
                r({
                    returned,
                })
            })
    })
}
