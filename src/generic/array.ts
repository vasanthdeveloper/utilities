/*
 *  Utility functions related to Arrays.
 *  Created On 08 January 2021
 */

export const removeDuplicates = array => Array.from(new Set(array))
