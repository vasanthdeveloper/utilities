/*
 *  This file contains some utility functions to manage execution timing.
 *  Created On 18 September 2020
 */

export const sleep = (milliseconds: number): Promise<void> => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

export const setInterval = async (
    milliseconds: number,
    func: any,
): Promise<void> => {
    while (true) {
        await sleep(milliseconds)
        await func()
    }
}
