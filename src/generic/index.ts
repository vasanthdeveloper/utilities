/*
 *  Utility functions safely usable in any JavaScript environment.
 *  Created On 07 July 2021
 */

export * as array from './array'
export * as promise from './promise'
export * as time from './time'
export * as object from './object'
